Répertoire pour [Moniliformopse](https://moniliformopse.aerobatic.io), un projet de site web et de wikis entièrement basé sur [TiddlyWiki](http://tiddlywiki.com/) (TW5) traduit en français.

Ce bloc-notes web interactif est accessible à l’adresse : [https://moniliformopse.aerobatic.io](https://moniliformopse.aerobatic.io)

Il regroupe un ensemble de ressources destinées à l’enseignement des Sciences de la vie et de la Terre (SVT).

**Moniliformopse** fait référence à un groupe de végétaux de la lignée verte dont voici quelques représentants : les prêles des champs (*Equisetum arvense*) de la famille des Sphénophytes ; le polypode commun (*Polypodium vulgare*), l’osmonde royale (*Osmunda regalis*) et la fougère aigle (*Pteridium aquilinum*) tous les trois de la famille des Filicophytes.